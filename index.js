let api = require('@actual-app/api');

(async () => {
  await api.init({
    // Budget data will be cached locally here, in subdirectories for each file.
    dataDir: '/tmp/actual',
    // This is the URL of your running server
    serverURL: 'http://localhost:5006',
    // This is the password you use to log into the server
    password: 'test',
  });

  // This is the ID from Settings → Show advanced settings → Sync ID
  await api.downloadBudget('bcc48b25-0336-45c0-8898-57068df35fe6');

  let budget = await api.getBudgetMonth('2019-10');
  console.log(budget);
  await api.shutdown();
})();